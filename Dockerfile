FROM ubuntu:22.04

# Install necessary packages
RUN apt-get update \
    && apt-get install -y \
       tzdata \
       apache2 \
       curl \
       dnsmasq \
       gcc \
       git \
       libapache2-mod-php \
       libssl-dev \
       lftp \
       make \
       mariadb-client \
       nfs-kernel-server \
       openssh-server \
       php \
       php-bcmath \
       php-cli \
       php-curl \
       php-gd \
       php-json \
       php-ldap \
       php-mbstring \
       php-mysql \
       php-pgsql \
       php-snmp \
       php-soap \
       php-xml \
       php-xmlrpc \
       php-zip \
       tar \
       tftp-hpa \
       wget \
       xinetd \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install FOGServer 1.5.9
RUN git clone --branch 1.5.9 https://github.com/FOGProject/fogproject.git /opt/fogproject \
    && cd /opt/fogproject/bin \
    && ./installfog.sh -y \
       --apache24 \
       --php7 \
       --no-ssl \
       --no-systemd \
       --installrouter \
       --foguser=fog \
       --foggroup=fog

# Expose necessary ports
EXPOSE 80/tcp
EXPOSE 69/udp
EXPOSE 111/tcp
EXPOSE 2049/tcp
EXPOSE 22/tcp

COPY fog /opt/fogproject/bin/

# Start FOG services
CMD ["/opt/fogproject/bin/fog", "start"]
